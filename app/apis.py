from flask.ext.restful import Api

from app import app

base = Api(app)
mgmt = Api(app, prefix='/mgmt')
twilio = Api(app, prefix='/twilio')
