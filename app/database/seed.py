from app import db, models

db.drop_all()
db.create_all()

entities = [
    {'name': 'Some Entitiy'},
    {'name': 'Some Other Entity'},
]
users = [
    {'first_name': 'Jeff', 'last_name': 'Zamanski', 'email': 'jzamanski@gmail.com', 'endpoint': '+19083687592',
     'number': '+19082900894', 'account_sid': 'ACf51d0531eabead70fa3eec5e9417cd6b',
     'auth_token': '80c27a3cfeffc096f8b40b90c21c79a6'}, #'+13475999066'
    {'first_name': 'Danny', 'last_name': 'Wnek', 'email': 'dannywnek@gmail.com', 'endpoint': '+19083479276',
     'number': '+16467130518', 'account_sid': 'ACf51d0531eabead70fa3eec5e9417cd6b',
     'auth_token': '80c27a3cfeffc096f8b40b90c21c79a6'}, #'+16463626799'
    {'first_name': 'Kathryn', 'last_name': 'Angeles', 'email': 'angeles.kathryn@gmail.com', 'endpoint': '+19088847150',
     'number': '+17322349839', 'account_sid': 'ACf51d0531eabead70fa3eec5e9417cd6b',
     'auth_token': '80c27a3cfeffc096f8b40b90c21c79a6'}, #'+19082645257'
]

ent1 = models.Entity(name=entities[0]['name'])
ent2 = models.Entity(name=entities[1]['name'])
db.session.add(ent1)
db.session.add(ent2)

for mem in users:
    user = models.User(first_name=mem['first_name'], last_name=mem['last_name'], email=mem['email'])
    if mem['first_name'] != 'Kathryn':
        user.entities.append(ent1)
    else:
        user.entities.append(ent2)
    user.hash_password(mem['email'])
    db.session.add(user)
    number = models.PhoneNumber(user=user, number=mem['number'],
                                account_sid=mem['account_sid'], auth_token=mem['auth_token'])
    db.session.add(number)
    endpoint = models.Endpoint(number=mem['endpoint'], user=user)
    db.session.add(endpoint)
db.session.commit()
