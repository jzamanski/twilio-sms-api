from app import app

if app.config['REBUILD_DATABASE']:
    import seed
