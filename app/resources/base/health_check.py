from flask_restful import Resource

from app import apis


class HealthCheck(Resource):
    def get(self):
        return "API is running..."


apis.base.add_resource(HealthCheck, '/health_check')
