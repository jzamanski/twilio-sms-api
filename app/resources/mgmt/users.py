from flask import g
from flask_restful import Resource, marshal_with, fields
from flask_wtf import Form
from wtforms import StringField
from wtforms.validators import DataRequired

from app import database
from app import models, apis, auth


class UserCreateForm(Form):
    email = StringField('email', validators=[DataRequired()])
    password = StringField('password', validators=[DataRequired()])


class Users(Resource):
    def get(self, id=None):
        return self.get_instance(id) if id else self.get_list()

    marshal_fields = {
        'id': fields.String,
        'first_name': fields.String,
        'last_name': fields.String,
        'email': fields.String,
    }

    @auth.token_required
    @marshal_with(marshal_fields)
    def get_list(self):
        return models.User.query.all()

    @auth.token_required
    @marshal_with(marshal_fields)
    def get_instance(self, id):
        if id == 'me':
            return g.user
        else:
            return models.User.query.get(id)
        return 'User does not exist.', 404


class UsersEntities(Resource):
    marshal_fields = {'id': fields.String}

    @auth.token_required
    @marshal_with(marshal_fields)
    def get(self, id):
        user = models.User.query.get(id)
        return user.entities


apis.mgmt.add_resource(Users, '/users/', '/users/<string:id>')
apis.mgmt.add_resource(UsersEntities, '/users/<string:id>/entities')


def post_users(username, password):
    db_session = database.Session()
    if db_session.query(models.User).filter(models.User.email == username).first() is not None:
        return 'user exists', 400
    user = models.User(email=username)
    user.hash_password(password)
    db_session.add(user)
    db_session.commit()
    return {'username': user.email, 'pass_hash': user.password_hash}, 201
