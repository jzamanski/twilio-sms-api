from flask import g
from flask_restful import Resource
from flask_wtf import Form
from wtforms import StringField
from wtforms.validators import DataRequired

from app import apis, auth


class TokenCreateForm(Form):
    email = StringField('email', validators=[DataRequired()])
    password = StringField('password', validators=[DataRequired()])


class Tokens(Resource):

    @auth.userpass.login_required
    def post(self):
        auth_token = g.user.generate_auth_token()
        import time # TODO remove, maybe, or keep to deter brute force?
        time.sleep(0.5)
        return {'auth_token': auth_token, 'user_id': g.user.id}

    @auth.token_required
    def get(self):
        return {'user_id': g.user.id}

    @auth.token_required
    def delete(self):
        return 'logout successful, i guess, not sure if anything needs to be done server side, client should fprget the token'


apis.mgmt.add_resource(Tokens, '/tokens/')
