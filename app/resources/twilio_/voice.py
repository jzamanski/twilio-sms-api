from flask_restful import Resource

from app import apis, auth


class TwilioVoice(Resource):
    @auth.twilio.login_required
    def get(self):
        return '<?xml version="1.0" encoding="UTF-8"?><Response><Say>HELLO WORLD!</Say></Response>'


apis.twilio.add_resource(TwilioVoice, '/twilio/voice')
