from flask import request
from flask_restful import Resource
from twilio.rest import TwilioRestClient
from wtforms import Form
from wtforms import StringField
from wtforms.validators import DataRequired

from app import models, db, apis, auth


class TwilioSMSForm(Form):
    From = StringField('From', validators=[DataRequired()])
    To = StringField('To', validators=[DataRequired()])
    Body = StringField('Body', validators=[DataRequired()])


class TwilioSMS(Resource):
    @auth.twilio.login_required
    def get(self):
        form = TwilioSMSForm(request.args)
        if not form.validate():
            return 'validation error', 404

        from_user = db.session.query(models.Endpoint).filter(models.Endpoint.number == form.From.data).first().user
        to_user = db.session.query(models.PhoneNumber).filter(models.PhoneNumber.number == form.To.data).first().user

        # since we have different accounts we need to send from the sender's account right now
        client = TwilioRestClient(from_user.phone_numbers[0].account_sid, from_user.phone_numbers[0].auth_token)
        message = client.messages.create(from_=from_user.phone_numbers[0].number, to=to_user.endpoints[0].number,
                                         body=form.Body.data)

        return '<?xml version="1.0" encoding="UTF-8"?><Response><Say>MESSAGE SENT</Say></Response>'


apis.twilio.add_resource(TwilioSMS, '/twilio/sms')
