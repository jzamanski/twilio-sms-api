from functools import wraps

from flask import g, request
from flask_httpauth import HTTPBasicAuth

from app import app, models

twilio = HTTPBasicAuth()
userpass = HTTPBasicAuth()
token = HTTPBasicAuth()


@twilio.verify_password
def verify_twilio_auth(username, password):
    return username == app.config['TWILIO_AUTH_USERNAME'] and password == app.config['TWILIO_AUTH_PASSWORD']


@userpass.verify_password
def verify_user_auth(email, password):
    users = models.User.query.filter_by(email=email)
    if users.count() > 0:
        user = users.first()
        if user.verify_password(password):
            g.user = user
            return True
    return False


def token_required(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        if 'Authorization' in request.headers:
            g.user = models.User.verify_auth_token(request.headers['Authorization'])
            if g.user:
                return f(*args, **kwargs)
        return 'Not Authorized', 401
    return decorated
