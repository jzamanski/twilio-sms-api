import uuid

from app import db


class Message(db.Model):
    id = db.Column(db.String(32), default=lambda: uuid.uuid4().hex, primary_key=True)
    content = db.Column(db.String(64))
    sender_user_id = db.Column(db.String(32), db.ForeignKey('user.id'))
    recipient_user_id = db.Column(db.String(32), db.ForeignKey('user.id'))

    def __repr__(self):
        return "<Message(content='%s')>".format(self.content)
