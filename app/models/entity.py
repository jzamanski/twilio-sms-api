import uuid

from flask_restful import fields

from app import db


class Entity(db.Model):
    id = db.Column(db.String(32), default=lambda: uuid.uuid4().hex, primary_key=True)
    name = db.Column(db.String(64))

    users = db.relationship('User', secondary='entity_user', back_populates='entities')

    def __repr__(self):
        return "<Entity(name='%s')>".format(self.name)
