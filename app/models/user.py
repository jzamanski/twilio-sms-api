import uuid

from itsdangerous import (TimedJSONWebSignatureSerializer as Serializer, BadSignature, SignatureExpired)

from app import app, bcrypt, db


class User(db.Model):
    id = db.Column(db.String(32), default=lambda: uuid.uuid4().hex, primary_key=True)
    entity_id = db.Column(db.String(32), db.ForeignKey('entity.id'))
    email = db.Column(db.String(64), unique=True)
    password_hash = db.Column(db.String(128))
    first_name = db.Column(db.String(64))
    last_name = db.Column(db.String(64))

    entities = db.relationship('Entity', secondary='entity_user', back_populates='users')
    endpoints = db.relationship("Endpoint", back_populates="user")
    phone_numbers = db.relationship("PhoneNumber", back_populates="user")

    def __repr__(self):
        return "<User(id='%s' first_name='%s', last_name='%s')>" % (self.id, self.first_name, self.last_name)

    def hash_password(self, password):
        self.password_hash = bcrypt.generate_password_hash(password)

    def verify_password(self, password):
        return bcrypt.check_password_hash(self.password_hash, password)

    def generate_auth_token(self, expiration=app.config['AUTH_TOKEN_EXPIRATION']):
        s = Serializer(app.config['SECRET_KEY'], expires_in=expiration)
        return s.dumps({'id': self.id})

    @staticmethod
    def verify_auth_token(token):
        s = Serializer(app.config['SECRET_KEY'])
        try:
            data = s.loads(token)
        except SignatureExpired:
            return None  # valid token, but expired
        except BadSignature:
            return None  # invalid token
        return User.query.get(data['id'])
