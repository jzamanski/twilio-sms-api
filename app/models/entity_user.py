from app import db

entity_user = db.Table('entity_user',
                       db.Column('entity_id', db.String(32), db.ForeignKey('entity.id')),
                       db.Column('user_id', db.String(32), db.ForeignKey('user.id'))
                       )
