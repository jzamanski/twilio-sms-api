import uuid

from app import db


class Endpoint(db.Model):
    id = db.Column(db.String(32), default=lambda: uuid.uuid4().hex, primary_key=True)
    user_id = db.Column(db.String(32), db.ForeignKey('user.id'))
    number = db.Column(db.String(32))

    user = db.relationship("User", back_populates="endpoints")

    def __repr__(self):
        return "<Endpoint(number='%s')>".format(self.number)
