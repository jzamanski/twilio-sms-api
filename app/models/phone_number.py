import uuid

from app import db


class PhoneNumber(db.Model):
    id = db.Column(db.String(32), default=lambda: uuid.uuid4().hex, primary_key=True)
    user_id = db.Column(db.String(32), db.ForeignKey('user.id'))
    number = db.Column(db.String(32))
    account_sid = db.Column(db.String(64))
    auth_token = db.Column(db.String(64))

    user = db.relationship("User", back_populates="phone_numbers")

    def __repr__(self):
        return "<PhoneNumber(number='%s')>".format(self.number)
