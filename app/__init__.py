# flask
from flask import Flask
app = Flask(__name__)
import configs

# flask-bcrypt
from flask.ext.bcrypt import Bcrypt
bcrypt = Bcrypt(app)

# flask-sqlalchemy
from flask.ext.sqlalchemy import SQLAlchemy
db = SQLAlchemy(app)
import database

# flask-httpauth
import auth

# flask-restful
import apis
import resources

# cross origin resource sharing
@app.after_request
def after_request(response):
    response.headers.add('Access-Control-Allow-Origin', '*')
    response.headers.add('Access-Control-Allow-Headers', 'Content-Type,Authorization')
    response.headers.add('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE')
    return response