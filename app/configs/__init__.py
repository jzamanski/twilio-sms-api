import os

from app import app

# No Environment
if 'APP_ENVIRONMENT' not in os.environ:
    exit("Environment variable 'APP_ENVIRONMENT' not defined.")

# Local/Dev/QA/Prod Environments
elif os.environ['APP_ENVIRONMENT'] in ['local','dev','qa','prod']:
    app.config.from_object('app.configs.' + os.environ['APP_ENVIRONMENT'])
    print 'Twilio SMS App Environment: ' + os.environ['APP_ENVIRONMENT']

# Unknown Environment
else:
    exit("Environment variable 'APP_ENVIRONMENT' value unknown.")
