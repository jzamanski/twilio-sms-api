import os

DEBUG = False

SECRET_KEY = os.environ['SECRET_KEY']

TWILIO_AUTH_USERNAME = os.environ['TWILIO_USERNAME']
TWILIO_AUTH_PASSWORD = os.environ['TWILIO_PASSWORD']

AUTH_TOKEN_EXPIRATION = 3600  # seconds

REBUILD_DATABASE = False
SQLALCHEMY_DATABASE_URI = "mysql+pymysql://{}:{}@{}:{}/{}".format(os.environ['DB_USERNAME'], os.environ['DB_PASSWORD'],
                                                                  os.environ['DB_HOSTNAME'], os.environ['DB_PORT'],
                                                                  os.environ['DB_NAME'])
SQLALCHEMY_TRACK_MODIFICATIONS = False

WTF_CSRF_ENABLED = False
