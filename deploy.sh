if [ $# -ne 1 ] || [[ "$1" != 'dev' && "$1" != 'qa' && "$1" != 'prod' ]]
then
    echo "Usage: `basename $0` dev|qa|prod"
    exit 1
fi

rm -rf web_app
mkdir web_app
cp -r ../web/app/* web_app/
sed -i '' 's/http:\/\/localhost:5000//g' web_app/app.js

if [ "$1" = 'dev' ]
then
    scp ~/.ssh/id_rsa root@mgmt.zam.me:~/.ssh/
    ssh root@mgmt.zam.me 'mkdir ~/twilio'
    rsync --delete -az ~/Dropbox/coding_projects/twilio/api root@mgmt.zam.me:~/twilio
    ssh -A root@mgmt.zam.me 'ssh -A web1 mkdir /var/www/html/twilio-sms-dev.zamanski.me'
    ssh -A root@mgmt.zam.me 'rsync --delete --exclude venv -az ~/twilio/api/ web1:/var/www/html/twilio-sms-dev.zamanski.me'
    ssh -A root@mgmt.zam.me 'ssh -A web1 cd /var/www/html/twilio-sms-dev.zamanski.me \&\& virtualenv-2.7 venv \&\& source venv/bin/activate \&\& pip2.7 install -r requirements.txt'
    ssh -A root@mgmt.zam.me 'ssh -A web1 service httpd restart'
    ssh root@mgmt.zam.me 'rm -f ~/.ssh/id_rsa'
fi

if [[ "$1" = 'qa' || "$1" = 'prod' ]]
then
    eb use twilio-sms-$1
    eb deploy --staged
fi

rm -rf web_app
