import os

# Local Environment
if __name__ == '__main__':
    import env_vars
    env_vars.load('local')
    import app
    app.app.run()

# Dev Environment
elif 'APP_ENVIRONMENT' not in os.environ:
    import sys, site
    base_path = os.path.dirname(os.path.abspath(__file__))
    sys.path.append(base_path)
    import env_vars
    env_vars.load('dev')
    site.addsitedir(os.path.join(base_path, 'venv/local/lib/python2.7/site-packages'))
    activate_file = os.path.join(base_path, 'venv/bin/activate_this.py')
    execfile(activate_file, dict(__file__=activate_file))
    import app
    application = app.app

# QA & Prod Environments
else:
    import app
    application = app.app
